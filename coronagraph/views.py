from django.shortcuts import render
import json

# Create your views here.

from django.http import HttpResponse
from django.template import loader
from coronagraph.models import StateWise, StateWiseTestedNumber, DailyRawData
from django.db.models import Sum, Count


def state_wise_graph(request):
    #template = loader.get_template('coronagraph/index.html')
    state_data_obj = StateWise.objects.all().order_by('-confirmed')[:11]
    state = []
    active = []
    recovered = []
    death = []
    confirmed = []
    for state_data in state_data_obj:
        state.append(state_data.state)
        active.append(state_data.active)
        recovered.append(state_data.recovered)
        death.append(state_data.deaths)
        confirmed.append(state_data.confirmed)
    context = {
        "state" : json.dumps(state),
        "active" : json.dumps(active),
        "recovered" : json.dumps(recovered),
        "death" : json.dumps(death),
        "confirmed" : json.dumps(confirmed)
    }
    return render(request, 'coronagraph/index.html', context)


def state_wise_tested_number(request):
    state = []
    percentage_postive = []
    state_data_obj = StateWiseTestedNumber.objects.values('state').order_by('state').annotate(Sum('total_tested')).annotate(Sum('positive'))
    for state_data in state_data_obj:
        state.append(state_data['state'])
        per = round((state_data['positive__sum'] / state_data['total_tested__sum']) * 100, 2)
        percentage_postive.append(per)
    context = {
                'state' : state,
                'percentage': percentage_postive
            }
    return render(request, 'coronagraph/positive_case.html', context)


def gender_wise_graphs(request):
    gender = ["Male", "Female"]
    male_hospitalized = 0
    male_recovered = 0
    male_deceased = 0
    female_hospitalized = 0
    female_recovered = 0
    female_deceased = 0
    state_data_obj = DailyRawData.objects.all().filter(gender__in=['M', 'F'])

    for state_data in state_data_obj:
        if state_data.gender == 'M':
            if state_data.current_status == "Hospitalized":
               male_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                male_recovered +=1
            elif state_data.current_status == "Deceased": 
                male_deceased +=1
        else:
            if state_data.current_status == "Hospitalized":
               female_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                female_recovered +=1
            elif state_data.current_status == "Deceased": 
                female_deceased +=1
    context = {
                'gender' : gender,
                'hospitalized': [male_hospitalized, female_hospitalized],
                'recovered' : [male_recovered, female_recovered],
                'deceased' : [male_deceased, female_deceased]
            }
    return render(request, 'coronagraph/gender_graph.html', context)



def age_group_wise_graphs(request):
    age_groups = ["0-10", "11-20", "21-30", "31-40", "41-50", "51-60", "61-70", "71-80", "81-90", "91-100"]
    _0_10_hospitalized = 0
    _11_20_hospitalized = 0
    _21_30_hospitalized = 0
    _31_40_hospitalized = 0
    _41_50_hospitalized = 0
    _51_60_hospitalized = 0
    _61_70_hospitalized = 0
    _71_80_hospitalized = 0
    _81_90_hospitalized = 0
    _91_100_hospitalized = 0
    _0_10_recovered = 0
    _11_20_recovered = 0
    _21_30_recovered = 0
    _31_40_recovered = 0
    _41_50_recovered = 0
    _51_60_recovered = 0
    _61_70_recovered = 0
    _71_80_recovered = 0
    _81_90_recovered = 0
    _91_100_recovered = 0
    _0_10_deceased = 0
    _11_20_deceased = 0
    _21_30_deceased = 0
    _31_40_deceased = 0
    _41_50_deceased = 0
    _51_60_deceased = 0
    _61_70_deceased = 0
    _71_80_deceased = 0
    _81_90_deceased = 0
    _91_100_deceased = 0

    state_data_obj = DailyRawData.objects.all().exclude(age=0)

    for state_data in state_data_obj:
        if state_data.age > 0 and state_data.age < 11 :
            if state_data.current_status == "Hospitalized":
               _0_10_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                _0_10_recovered +=1
            elif state_data.current_status == "Deceased": 
                _0_10_deceased +=1
        elif state_data.age > 10 and state_data.age < 21: 
            if state_data.current_status == "Hospitalized":
               _11_20_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                _11_20_recovered +=1
            elif state_data.current_status == "Deceased": 
                _11_20_deceased +=1
        elif state_data.age > 20 and state_data.age < 31: 
            if state_data.current_status == "Hospitalized":
               _21_30_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                _21_30_recovered +=1
            elif state_data.current_status == "Deceased": 
                _21_30_deceased +=1
        elif state_data.age > 30 and state_data.age < 41: 
            if state_data.current_status == "Hospitalized":
               _31_40_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                _31_40_recovered +=1
            elif state_data.current_status == "Deceased": 
                _31_40_deceased +=1
        elif state_data.age > 40 and state_data.age < 51: 
            if state_data.current_status == "Hospitalized":
               _41_50_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                _41_50_recovered +=1
            elif state_data.current_status == "Deceased": 
                _41_50_deceased +=1
        elif state_data.age > 50 and state_data.age < 61: 
            if state_data.current_status == "Hospitalized":
               _51_60_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                _51_60_recovered +=1
            elif state_data.current_status == "Deceased": 
                _51_60_deceased +=1
        elif state_data.age > 60 and state_data.age < 71: 
            if state_data.current_status == "Hospitalized":
               _61_70_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                _61_70_recovered +=1
            elif state_data.current_status == "Deceased": 
                _61_70_deceased +=1
        elif state_data.age > 70 and state_data.age < 81: 
            if state_data.current_status == "Hospitalized":
               _71_80_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                _71_80_recovered +=1
            elif state_data.current_status == "Deceased": 
                _71_80_deceased +=1
        elif state_data.age > 80 and state_data.age < 91: 
            if state_data.current_status == "Hospitalized":
               _81_90_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                _81_90_recovered +=1
            elif state_data.current_status == "Deceased": 
                _81_90_deceased +=1
        elif state_data.age > 90 and state_data.age < 101: 
            if state_data.current_status == "Hospitalized":
               _91_100_hospitalized +=1
            elif state_data.current_status == "Recovered": 
                _91_100_recovered +=1
            elif state_data.current_status == "Deceased": 
                _91_100_deceased +=1

    context = {
                'age_groups' : age_groups,
                'hospitalized': [_0_10_hospitalized, _11_20_hospitalized,  _21_30_hospitalized , _31_40_hospitalized ,
    _41_50_hospitalized ,
    _51_60_hospitalized ,
    _61_70_hospitalized ,
    _71_80_hospitalized ,
    _81_90_hospitalized ,
    _91_100_hospitalized ],
                'recovered' : [_0_10_recovered ,
    _11_20_recovered ,
    _21_30_recovered ,
    _31_40_recovered ,
    _41_50_recovered ,
    _51_60_recovered ,
    _61_70_recovered ,
    _71_80_recovered ,
    _81_90_recovered ,
    _91_100_recovered ],
                'deceased' : [ _0_10_deceased ,
    _11_20_deceased ,
    _21_30_deceased ,
    _31_40_deceased ,
    _41_50_deceased ,
    _51_60_deceased ,
    _61_70_deceased ,
    _71_80_deceased ,
    _81_90_deceased ,
    _91_100_deceased ]
            }
    return render(request, 'coronagraph/age_wise_graph.html', context)