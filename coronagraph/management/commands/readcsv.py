from django.core.management.base import BaseCommand, CommandError
from coronagraph.models import StateWise, StateWiseTestedNumber, DailyRawData
import csv
from django.db import transaction
from django.db import DatabaseError, transaction


class Command(BaseCommand):

    help = 'Command to insert csv data into database'
    
    def handle(self, *args, **options):
        # with open('coronagraph/static/state_wise.csv') as matches_file:
        #     match_data = csv.DictReader(matches_file)
        #     for match in match_data:
        #         data = StateWise(state=match['State'], confirmed=match['Confirmed'], recovered=match['Recovered'], deaths=match['Deaths'],
        #                      active=match['Active'], state_code=match['State_code'])
        #         data.save()
        # StateWiseTestedNumber.objects.all().delete()
        # with open('coronagraph/static/statewise_tested_numbers_data.csv') as matches_file:
        #     match_data = csv.DictReader(matches_file)
        #     for match in match_data:
        #         date_list = match['Updated On'].split("/")
        #         reverse_date_list = date_list.reverse()
        #         date = '-'.join(date_list)
        #         tested = match['Total Tested']
        #         positive = match['Positive']
        #         if not tested:
        #         	tested = 0
        #         if not positive:
        #         	positive = 0  

        #         data = StateWiseTestedNumber(state=match['State'], total_tested=tested, positive=positive, 
        #                      tested_date=date)
        #         data.save()

        # DailyRawData.objects.all().delete()

        with open('coronagraph/static/raw_data7.csv') as matches_file:
            match_data = csv.DictReader(matches_file)
            with transaction.atomic():
                for match in match_data:
                    date_list = match['Date Announced'].split("/")
                    reverse_date_list = date_list.reverse()
                    date = '-'.join(date_list)
                    age = match['Age Bracket']
                    gender = match['Gender']
                    country = match['Nationality']
                    if not country or country != 'India':
                        continue
                    if not age and not gender:
                        continue
                    if not age:
                       age = 0 

                    data = DailyRawData(state=match['Detected State'], age=age, gender=gender, 
                                 date_announced=date, district=match['Detected District'], city=match['Detected City'], 
                                 state_code=match['State code'], current_status=match['Current Status'])
                    data.save()
        self.stdout.write("Csv to MongoDb data is inserted")