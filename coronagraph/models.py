from django.db import models

# Create your models here.

class StateWise(models.Model):
    state = models.CharField(max_length=200)
    state_code = models.CharField(max_length=10)
    confirmed = models.IntegerField()
    recovered = models.IntegerField()
    deaths = models.IntegerField()
    active = models.IntegerField()


class StateWiseTestedNumber(models.Model):
    state = models.CharField(max_length=200)
    total_tested = models.IntegerField()
    tested_date = models.DateField(blank=True, null=True)
    positive = models.IntegerField()


class DailyRawData(models.Model):
    age = models.IntegerField(blank=True, null=True)
    date_announced = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=10, blank=True, null=True)
    city = models.CharField(max_length=200, blank=True, null=True)
    district =  models.CharField(max_length=200, blank=True, null=True)
    state =  models.CharField(max_length=200, blank=True, null=True)
    state_code = models.CharField(max_length=200, blank=True, null=True)
    current_status =  models.CharField(max_length=200, blank=True, null=True)
