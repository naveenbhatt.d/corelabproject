from django.apps import AppConfig


class CoronagraphConfig(AppConfig):
    name = 'coronagraph'
