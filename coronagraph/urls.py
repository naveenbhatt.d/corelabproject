from django.urls import path

from . import views

urlpatterns = [
    path('', views.state_wise_graph, name='state_wise_graph'),
    path('positive-case', views.state_wise_tested_number, name='positive_case'),
    path('gender-wise-graph', views.gender_wise_graphs, name='gender_wise_graph'),
    path('age-wise-graph', views.age_group_wise_graphs, name='age_wise_graph'),

]